<section class="fw-main-row" style="padding: 40px 0;">
    <div class="fw-container full">
        <h1 align="center">Clipping</h1>
        <!-- Filter buttons -->
        <div class="fw-col-xs-12 fw-col-md-8 fw-col-lg-12 is-col tac">
            <p>Clipping and Multipath require dedication, concentration and precision And <br>we have the designers who have  all these virtues which help us provide you the best quality <br>Clipping and Multipath for your images in a very short period of time.
            </p>
        </div>
    </div>
    <br>
    <!-- END Filter buttons -->
    <div class="cf-items fw-row">
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion people" style="background-image: url(images/imgs/clipping/1.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 wedding people" style="background-image: url(images/imgs/clipping/2.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion nature" style="background-image: url(images/imgs/clipping/3.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 wedding nature" style="background-image: url(images/imgs/clipping/4.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion people" style="background-image: url(images/imgs/clipping/5.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion nature" style="background-image: url(images/imgs/clipping/6.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 city nature" style="background-image: url(images/imgs/clipping/7.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion city" style="background-image: url(images/imgs/clipping/8.jpg);">

        </div>

        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion city" style="background-image: url(images/imgs/clipping/9.jpg);">

        </div>

        <!-- END Gallery item -->

        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion city" style="background-image: url(images/imgs/clipping/10.jpg);">

        </div>
        <!-- Gallery item -->

        <!-- Gallery item -->
        <!-- END Gallery item -->

        <!-- END Gallery item -->
        <!-- END Gallery item -->
    </div>
</section>