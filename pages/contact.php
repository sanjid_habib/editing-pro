<section class="fw-main-row">
    <div class="fw-container">
        <h1 class="heading-decor">contact</h1>
    </div>
</section>
<!-- Map -->

<!-- END Map -->
<section class="fw-main-row" style="padding: 30px 0 50px;">
    <div class="fw-container">
        <div class="fw-row">
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-3">
                <div class="h5 heading-decor2">contacts</div>
                <!-- Contact rows -->
                <div class="contact-row">
                    <div class="label">Address:</div>
                    <div class="value">Uposhohor, Sylhet, Bangladesh</div>
                </div>
                <div class="contact-row">
                    <div class="label">Phone:</div>
                    <div class="value">+88 01672 502 505</div>
                </div>
                <div class="contact-row">
                    <div class="label">Enquiries:</div>
                    <div class="value"><a href="info@editingpic.com">info@editingpic.com</a></div>
                </div>
                <div class="contact-row">
                    <div class="label">Sales:</div>
                    <div class="value"><a href="markalburt@gmail.comm">markalburt@gmail.com</a></div>
                </div>
                <div class="contact-row">
                    <div class="label">Skype:</div>
                    <div class="value"><a href="markalburt@gmail.com">markalburt@gmail.com</a></div>
                </div>
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4" style="margin-bottom: 20px;">
                <h5 class="heading-decor2">follow us</h5>
                <!-- Social icons -->
                <div class="social-links">
                    <a href="javascript:void(0);"><i class="social-icons icon-facebook-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-twitter-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-pinterest-social-logo"></i></a>
                    <a href="javascript:void(0);"><i class="social-icons icon-youtube-logotype"></i></a>
                </div>
                <!-- END Social icons -->
            </div>
            <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4" style="margin-bottom: 20px;">
                <h5 class="heading-decor2">stay in touch</h5>
                <!-- Subscribe form -->
                <form action="javascript:void(0);" class="subscribe-form"><input type="text" placeholder="Enter your e-mail" name="email" class="input"><button type="submit" class="button-style1"><span>subscribe</span></button></form>
                <!-- END Subscribe form -->
            </div>
        </div>
        <div class="fw-row" style="padding: 50px 0 0;">
            <h2 class="heading-decor">We�d love to hear from you</h2>
            <p class="tac t-text">If you have any questions about our products or services,<br>our team is ready to answer on it.<br>So feel free to get in touch with our support team.</p>
            <!-- Feedback form -->
            <form action="pages/email.php" id="emailform" method="post" class="feedback-form fw-row" style="margin-top: 50px;">
                <div class="fw-col-xs-12 fw-col-sm-4">
                    <input type="text" id="name" name="name" placeholder="name" class="input-style1">
                    <input type="text" id="email" name="email" placeholder="e-mail" class="input-style1">
                    <input type="text" id="phone" name="phone" placeholder="phone" class="input-style1">
                </div>
                <div class="fw-col-xs-12 fw-col-sm-8">
                    <textarea id="message" name="message" cols="30" rows="10" placeholder="Message" class="textarea-style1"></textarea>
                </div>
                <div class="fw-col-xs-12 tac">
                    <input id="submit" type="button" value="Submit" class="button-style1">
                </div>
            </form>
            <!-- END Feedback form -->
        </div>
</section>