<section class="fw-main-row" style="padding: 40px 0;">
    <div class="fw-container full">
        <h1 align="center">Masking</h1>
        <!-- Filter buttons -->

        <p align="center">Masking is something which many professionals are not comfortable to do but we have some <br>really good designers who are really good with masking. We have a section  dedicated to get the masking job<br> done so you can get your masked out images in a very short period of time.  </p>


    </div>

    <br>
    <!-- END Filter buttons -->
    <div class="cf-items fw-row">
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion people" style="background-image: url(images/imgs/masking/1.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 wedding people" style="background-image: url(images/imgs/masking/2.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion nature" style="background-image: url(images/imgs/masking/3.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 wedding nature" style="background-image: url(images/imgs/masking/4.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion people" style="background-image: url(images/imgs/masking/5.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion nature" style="background-image: url(images/imgs/masking/6.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 city nature" style="background-image: url(images/imgs/masking/7.jpg);">

        </div>
        <!-- END Gallery item -->
        <!-- Gallery item -->
        <div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion city" style="background-image: url(images/imgs/masking/8.jpg);">

        </div>

        <!-- END Gallery item -->
    </div>
</section>