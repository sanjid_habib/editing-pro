$(document).ready(function(){
    $("#submit").click(function(){
        var name = $("#name").val();
        var email = $("#email").val();
        var phone = $("#phone").val();
        var message = $("#message").val();
// Returns successful data submission message when the entered information is stored in database.
        var dataString = 'name='+ name + '&email='+ email + '&phone='+ phone + '&message='+ message;
        if(name==''||email==''||phone==''||message=='')
        {
            alert("Please Fill All Fields");
        }
        else
        {
// AJAX Code To Submit Form.
            $.ajax({
                type: "POST",
                url: "pages/email.php",
                data: $('#emailform').serialize(),
                cache: false,
                success: function(result){
                    alert(result);
                }
            });
        }
        return false;
    });
});/**
 * Created by Sanjid on 6/16/2017.
 */
