
    <meta charset=utf-8>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editing-Home</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <!-- Bootstrap -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/email.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script type="text/javascript"src="js/bootstrap.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <!--css-->
    <link id="favicon" rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="144x144" href="images/favicon144.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/favicon114.png">
    <link rel="stylesheet" href="css/fonts.css">
    <link rel="stylesheet" href="css/icon-font.css">
    <link rel="stylesheet" href="css/social-icons.css">
    <link rel="stylesheet" href="css/frontend-grid.css">
    <link rel="stylesheet" href="js/owl.carousel/owl.carousel.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/mobile.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/mootools/1.4.0/mootools-yui-compressed.js"></script>
    <script type="text/javascript" src="js/selectivizr-min.js"></script>
    <script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- Page header -->
<header class="header dark-bg clearfix">
    <div class="fw-container">
        <div class="fw-main-row">
            <div class="fl logo-area"><a href="index.html"><img src="images/logo2.png" class="logo-dark" alt="Crop It"><img src="images/logo2-light.png" class="logo-light" alt="Crop It"></a></div>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" href="#collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar">-</span>
                <span class="icon-bar">-</span>
                <span class="icon-bar">-</span>
            </button>
            <div class="fr collapse navbar-collapse" id="collapse-1">
                <!-- Site navigation -->
                <nav class="navigation">
                    <ul>
                        <li class="menu-item-has-children" id="home">
                            <a href="index.php?pname=home">HOMe</a>
                        </li>
                        <li class="menu-item-has-children" id="retouch">
                            <a href="?pname=retouch">RETOUCH</a>
                        </li>
                        <li class="menu-item-has-children" id="masking">
                            <a href="?pname=masking">MASKING</a>
                        </li>
                        <li class="menu-item-has-children" id="nickjoint">
                            <a href="?pname=nickjoint">NICKJOINT</a>
                        </li>
                        <li class="menu-item-has-children" id="clipping">
                            <a href="?pname=clipping">CLIPPING</a>
                        </li>
                        <li class="menu-item-has-children" id="contact">
                            <a href="?pname=contact">CONTACT</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
    <div class="header-space"></div>
    <script>
        var liname="<?php echo $_GET['pname']?>"
        $('#'+liname).addClass('current-menu-item');
    </script>