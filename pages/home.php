<!-- END Page preloader -->
<!-- Full scren slider -->
<section class="full-screen-slider">
	<div class="item dark-bg" style="background-image: url(images/imgs/full-screen-slider/slide1.jpg);"></div>
	<div class="item dark-bg" style="background-image: url(images/imgs/full-screen-slider/slide2.jpg);"></div>
	<div class="item dark-bg" style="background-image: url(images/imgs/full-screen-slider/slide5.jpg);"></div>
	<div class="item dark-bg" style="background-image: url(images/imgs/full-screen-slider/slide6.jpg);"></div>
	<div class="item dark-bg" style="background-image: url(images/imgs/full-screen-slider/slide7.jpg);"></div>
	<div class="item dark-bg" style="background-image: url(images/imgs/full-screen-slider/slide8.jpg);"></div>
</section>
<!-- END Full scren slider -->
<!-- Info section -->
<section class="fw-main-row padding-on info-section" style="background: #ededed;">
	<div class="fw-container full">
		<!-- Info section image -->
		<div class="fw-col-xs-12 fw-col-md-4 fw-col-lg-6 is-col">
			<section class="full-screen-slider">
				<div class="item dark-bg" style="background-image: url(images/imgs/about1-p.jpg);">

				</div>
				<div class="item dark-bg" style="background-image: url(images/imgs/about2-p.jpg);">

				</div>
				<div class="item dark-bg" style="background-image: url(images/imgs/about4-p.jpg);">

				</div>
				<div class="item dark-bg" style="background-image: url(images/imgs/about3-p.jpg);">

				</div>
				<div class="item dark-bg" style="background-image: url(images/imgs/about5-p.jpg);">

				</div>
				<div class="item dark-bg" style="background-image: url(images/imgs/about6-p.jpg);">

				</div>
				<div class="item dark-bg" style="background-image: url(images/imgs/about7-p.jpg);">

				</div>
				<div class="item dark-bg" style="background-image: url(images/imgs/about8-p.jpg);">

				</div>
				<div class="item dark-bg" style="background-image: url(images/imgs/about9-p.jpg);">

				</div>
				<div class="item dark-bg" style="background-image: url(images/imgs/about10-p.jpg);">

				</div>
		</div>


		<section>

			<!-- END Info section image -->
			<!-- Info section text -->
			<div class="fw-col-xs-12 fw-col-md-8 fw-col-lg-6 is-col tac">
				<div class="is-text clearfix">
					<br>
					<br>
					<br>

					<h1>Editing Pic</h1>

					<p>EditingPic is one of the fastest growing image editing service provider. We are here to help
						you with all the possible image editing services, which can be done by Photoshop.
						Our main concern is to give you the �better� services and to become �best� editing service
						provider.
						We are here to give you all the solutions regarding Image Retouching, Masking, Neck
						Joint/Image Ghosting, Clipping etc with the best price possible and we can assure you that
						there will be no bargain with the quality of your images.
					</p>
				</div>
			</div>
			<!-- END Info section text -->
	</div>
</section>
<!-- END Info section -->
<!-- Portfolio categories -->

<!-- END Portfolio categories -->
<!-- Instagram photo slider -->
<section class="fw-main-row dark-bg" style="padding: 30px 0 0;">
	<div class="fw-container full">
		<div class="fw-col-xs-12">
			<h2><span>Editing  Pic</span> <a href="https://twitter.com/MarkAlburt;" class="button-style1 fr hd-btn"><i
						class="social-icons icon-instagram-social-network-logo-of-photo-camera"></i><span>follow us</span></a>
			</h2>
		</div>
		<div class="fw-row">
			<div class="ig-carousel">
				<div class="item" style="background-image: url(images/imgs/instagram/1.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/2.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/3.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/4.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/5.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/6.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/7.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/8.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/9.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/10.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/11.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/12.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/13.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/14.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/15.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/16.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/17.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/18.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/19.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/20.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/21.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/22.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/23.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/24.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/25.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/26.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/27.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/28.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/29.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/30.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/31.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/32.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/33.jpg);"><a
						href="javascript:void(0);"></a></div>
				<div class="item" style="background-image: url(images/imgs/instagram/34.jpg);"><a
						href="javascript:void(0);"></a></div>


			</div>
		</div>
	</div>
</section>
<!-- END Instagram photo slider -->
<!-- Content -->


