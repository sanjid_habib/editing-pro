<!DOCTYPE html>
<html lang="en">
<head>
	<?php @include('includes/header.php'); ?>
</head>
<body>
<div id="page">
	<!-- Page preloader -->
	<div id="page-preloader">
		<div class="spinner centered-container"></div>
	</div>
	<?php
	if (isset($_GET['pname']))
    {
		$page = $_GET['pname'];
		if ($page == 'retouch') {
			@include('pages/retouch.php');
		} elseif ($page == 'masking') {
			@include('pages/masking.php');
		}
		elseif ($page == 'nickjoint') {
			@include('pages/nickjoint.php');
		}
		elseif ($page == 'clipping') {
			@include('pages/clipping.php');
		}
		elseif ($page == 'contact') {
			@include('pages/contact.php');
		}
		else {
			@include('pages/home.php');
		}
	}
	else
	{
		@include('pages/home.php');
	}
	?>

</div>
</body>
<?php @include('includes/footer.php'); ?>
</html>
