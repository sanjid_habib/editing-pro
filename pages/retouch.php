
<section class="fw-main-row" style="padding: 40px 0;">
	<div class="fw-container full">
		<h1 align="center">Retouching</h1>
		<!-- Filter buttons -->
		<div class="fw-col-xs-12 fw-col-md-8 fw-col-lg-12 is-col tac">
			<p>Retouching is one of the very sensitive works of Photoshops which requires a lot of attention<br> and we do it with great care so you can get the best quality of your reprocessed photos edited by us.<br> We have some example for you which will help you to understand the quality of our work.
			</p>

		</div>
	</div>
	<br>
	<!-- END Filter buttons -->
	<div class="cf-items fw-row">
		<!-- Gallery item -->
		<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion people" style="background-image: url(images/imgs/gallery/1.jpg);">

		</div>
		<!-- END Gallery item -->
		<!-- Gallery item -->
		<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 wedding people" style="background-image: url(images/imgs/gallery/2.jpg);">

		</div>
		<!-- END Gallery item -->
		<!-- Gallery item -->
		<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion nature" style="background-image: url(images/imgs/gallery/3.jpg);">

		</div>
		<!-- END Gallery item -->
		<!-- Gallery item -->
		<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 wedding nature" style="background-image: url(images/imgs/gallery/4.jpg);">

		</div>
		<!-- END Gallery item -->
		<!-- Gallery item -->
		<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion people" style="background-image: url(images/imgs/gallery/5.jpg);">

		</div>
		<!-- END Gallery item -->
		<!-- Gallery item -->
		<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion nature" style="background-image: url(images/imgs/gallery/6.jpg);">

		</div>
		<!-- END Gallery item -->
		<!-- Gallery item -->
		<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 city nature" style="background-image: url(images/imgs/gallery/7.jpg);">

		</div>
		<!-- END Gallery item -->
		<!-- Gallery item -->
		<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion city" style="background-image: url(images/imgs/gallery/8.jpg);">

		</div>

		<!-- END Gallery item -->
		<!-- Gallery item -->
		<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion city" style="background-image: url(images/imgs/gallery/9.jpg);">

		</div>
		<div class="cf-item gallery-item fw-col-xs-12 fw-col-sm-6 fashion city" style="background-image: url(images/imgs/gallery/10.jpg);">

		</div>

		<!-- END Gallery item -->

		<!-- Gallery item -->

		<!-- Gallery item -->
		<!-- END Gallery item -->

		<!-- END Gallery item -->

		<!-- END Gallery item -->
	</div>
</section>