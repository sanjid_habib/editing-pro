<footer class="footer dark-bg">
    <div class="fw-container">
        <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-3">
            <div class="h5">contacts</div>
            <!-- Contact rows -->
            <div class="contact-row">
                <div class="label">Address:</div>
                <div class="value">Sylhet</div>
            </div>
            <div class="contact-row">
                <div class="label">Phone:</div>
                <div class="value">+88 01672 502 505</div>
            </div>
            <div class="contact-row">
                <div class="label">Enquiries:</div>
                <div class="value"><a href="info@editingpic.com">info@editingpic.com</a></div>
            </div>
            <div class="contact-row">
                <div class="label">Sales:</div>
                <div class="value"><a href="markalburt@gmail.comm">markalburt@gmail.com</a></div>
            </div>
            <div class="contact-row">
                <div class="label">Skype:</div>
                <div class="value"><a href="markalburt@gmail.com">markalburt@gmail.com</a></div>
            </div>
        </div>
        <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-3">
            <div class="h5">gallery</div>
            <!-- Gallery thumbnails -->
            <div class="gallery-module fw-row">
                <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="images/imgs/gallery-module/img1.jpg" alt="gallery item 1"></a></div>
                <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="images/imgs/gallery-module/img2.jpg" alt="gallery item 2"></a></div>
                <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="images/imgs/gallery-module/img3.jpg" alt="gallery item 3"></a></div>
                <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="images/imgs/gallery-module/img4.jpg" alt="gallery item 4"></a></div>
                <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="images/imgs/gallery-module/img5.jpg" alt="gallery item 5"></a></div>
                <div class="fw-col-xs-4 fw-col-sm-4 item"><a href="javascript:void(0);"><img src="images/imgs/gallery-module/img6.jpg" alt="gallery item 6"></a></div>
            </div>
        </div>
        <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-2">
            <div class="h5">LINKS</div>
            <!-- Footer nav -->
            <nav class="footer-nav">
                <ul>
                    <li><a href="index.html">Home Page</a></li>
                    <li><a href="retouch.html">retouching</a></li>
                    <li><a href="clipping.html">clipping</a></li>
                    <li><a href="maksing.html">masking</a></li>
                    <li><a href="nickjoint.html">nickjoint</a></li>
                    <li><a href="contact.html">Contact</a></li>
                </ul>
            </nav>
        </div>
        <div class="fw-col-xs-12 fw-col-sm-6 fw-col-md-4">
            <div class="h5">stay in touch</div>
            <!-- Subscribe form -->
            <form action="javascript:void(0);" class="subscribe-form"><input type="text" placeholder="Enter your e-mail" class="input"><button type="submit" class="button-style1"><span>subscribe</span></button></form>
            <!-- Social links -->
            <div class="social-links">
                <span>follow us:</span>


                <a href="javascript:void(0);"><i class="social-icons icon-twitter-logo"></i></a>

            </div>
        </div>
    </div>
</footer>

<script src="js/owl.carousel/owl.carousel.min.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>

